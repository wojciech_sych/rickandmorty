import { Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { RickAndMorty, Character } from '../rickandmorty.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-character',
    templateUrl: './character.component.html',
    styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

    constructor(private route: ActivatedRoute, public _rickandmortyService: RickAndMorty) { }

    current: Character;
    preloader: boolean = true;

    ngOnInit() {
        this._rickandmortyService.subpage.next(true);
        this.loadCharacterDetail();
    }

    loadCharacterDetail() {     
        const id = +this.route.snapshot.paramMap.get('id');
        this._rickandmortyService.getCharacter(id);
        this._rickandmortyService.current.subscribe(c=>{
            this.current = c;
            this.preloader = false;
        });
    }

    ngOnDestroy() {
        delete this.current;
        this.preloader = true;
    }
}
