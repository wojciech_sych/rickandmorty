import { Component, Output } from '@angular/core';
import { RickAndMorty, Character } from './rickandmorty.service';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    subpage: boolean = false;

    characters: Observable<Character[]>;
    charactersList: Character[] = [];
    search: string = '';
    liveSearch: Character[] = [];

    constructor(public _rickandmortyService: RickAndMorty) {

    }

    ngOnInit() {
        this.characters = this._rickandmortyService.characters;
        this._rickandmortyService.getCharacters();

        this.characters.subscribe(data => this.charactersList = data);

        this._rickandmortyService.subpage.subscribe(d=> {
            setTimeout(() => { this.subpage = d; }, 0);
        });

        this._rickandmortyService.liveSearch.subscribe(d => this.liveSearch = d);
    }

    searchAction(e) {
        if (e.length > 2) {
            this._rickandmortyService.getCharacters(null, e);
        }
    }

}
