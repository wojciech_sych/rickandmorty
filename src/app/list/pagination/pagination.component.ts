import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface Pagination {
    start: number;
    stop: number;
    activeIndex: number;
    showOnPage: number;
    elements: number;
    count: number;
}

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
     @Input() pagination: Pagination; 
     @Output() getPage = new EventEmitter();

    
    clickPagination(p: number) {
        // this.pagination.start = p * this.pagination.showOnPage;
        // this.pagination.stop = this.pagination.start + this.pagination.showOnPage;
        this.getPage.emit(p + 1);
    }

    updatePagination() {
        this.pagination.elements = Math.ceil(this.pagination.count / this.pagination.showOnPage);
        this.pagination.start = 0;
        this.pagination.activeIndex = 0;
        this.pagination.stop = this.pagination.showOnPage;
    }

    constructor() { }

    ngOnInit() {
        
    }

}
