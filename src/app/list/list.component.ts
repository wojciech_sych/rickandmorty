import { Component, OnInit } from '@angular/core';
import { RickAndMorty, Character } from '../rickandmorty.service';
import { Observable } from 'rxjs';

import { Pagination } from './pagination/pagination.component';
import { ThrowStmt } from '@angular/compiler';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
    characters: Observable<Character[]>;
    charactersList: Character[] = [];

    sort: string = '';

    sortTable() {
        this.charactersList.sort((a, b) => {
            let compar = 0
            if (this.sort === 'genderDESC') {
                if (a.gender < b.gender) {
                    compar = -1;
                }
            } else if (this.sort === 'genderASC') {
                if (a.gender > b.gender) {
                    compar = -1;
                }
            } else if (this.sort === '') {
                if (a.id > b.id) {
                    compar = 1;
                } else {
                    compar = -1;
                }
            }

            return compar;
        });
    }

    clearFilters() {
        this.sort = '';
        this.sortTable();
    }

    pagination: Pagination = {
        start: 0,
        stop: 20,
        showOnPage: 20,
        activeIndex: 0,
        elements: 10,
        count: 200
    };

    constructor(public _rickandmortyService: RickAndMorty) { }


    ngOnInit() {
        this.characters = this._rickandmortyService.characters;
        this.characters.subscribe(data => {
            this.charactersList = data;
        });
    }

    getPage(id: number) {
        this._rickandmortyService.getCharacters(id);
    }
}
