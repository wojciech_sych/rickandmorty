import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterComponent } from './character/character.component';
import { ListComponent } from './list/list.component';


const routes: Routes = [
    { path: '', component: ListComponent },
    { path: 'character/:id', component: CharacterComponent },
    { path: '**', component: ListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
