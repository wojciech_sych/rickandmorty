import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export interface Character {
    id: number;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    origin: {
        name: string;
        url: string
    };
    location: {
        name: string;
        url: string
    };
    image: string;
    episode: string[];
    url: string;
    created: string;
}

export interface Info {
    count: number;
    pages: number;
    next: string;
    prev: string
}

export interface ApiList {
    info: Info;
    results: Character[]
}

@Injectable({
    providedIn: 'root'
})
export class RickAndMorty {

    constructor(private http: HttpClient) { }

    public subpage: Subject<boolean> = new Subject();

    private _characters: BehaviorSubject<Character[]> = new BehaviorSubject<Character[]>([]);
    readonly characters: Observable<Character[]> = this._characters.asObservable();
    private _liveSearch: BehaviorSubject<Character[]> = new BehaviorSubject<Character[]>([]);
    readonly liveSearch: Observable<Character[]> = this._liveSearch.asObservable();
    private _current: BehaviorSubject<Character> = new BehaviorSubject<Character>(null);
    readonly current: Observable<Character> = this._current.asObservable();

    private dataStore: { characters: Character[] } = { characters: [] };

    getCharacters(page: number | null = null, search?: string): void {
        let query = '';

        if (page !== null) {
            query = `?page=${page}`;
        }

        if (search !== undefined) {
            query = `?name=${search}`;
        }

        this.http.get<ApiList>(`https://rickandmortyapi.com/api/character/${query}`).subscribe(data => {
            if (search === undefined) {
                this.dataStore.characters = data.results;
                this._characters.next(Object.assign({}, this.dataStore).characters);
            } else {
                this._liveSearch.next(data.results);
            }
        });
    }

    getCharacter(id) {
        this.http.get<Character>(`https://rickandmortyapi.com/api/character/${id}`).subscribe(data => {
            this._current.next(data);
        });
    }
}